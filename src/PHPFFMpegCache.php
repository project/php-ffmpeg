<?php

namespace Drupal\php_ffmpeg;

use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\Cache\Adapter\AbstractAdapter;

/**
 * Adapter between Symfony cache needed by FFMPeg library and Drupal cache.
 */
class PHPFFMpegCache extends AbstractAdapter {

  /**
   * The cache backend that should be used.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a CacheCollector object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param string $prefix
   *   Prefix used for appending to cached item identifiers.
   */
  public function __construct(CacheBackendInterface $cache, $prefix) {
    parent::__construct($prefix);
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  protected function doFetch(array $ids): iterable {
    $items = $this->cache->getMultiple($ids);
    array_walk($items, function (&$item) {
      $item = $item->data;
    });
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function doHave(string $id): bool {
    return ($this->cache->get($id) !== FALSE);
  }

  /**
   * {@inheritdoc}
   */
  protected function doSave(array $values, int $lifetime): array|bool {
    array_walk($values, function (&$value) use ($lifetime) {
      $value = [
        "data" => $value,
        "expire" => ($lifetime == 0) ? CacheBackendInterface::CACHE_PERMANENT : time() + $lifetime
      ];
    });
    $this->cache->setMultiple($values);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function doDelete(array $ids): bool {
    $this->cache->deleteMultiple($ids);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function doClear(string $namespace): bool {
    $this->cache->deleteAll();
    return TRUE;
  }

}
