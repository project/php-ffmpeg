<?php

namespace Drupal\Tests\php_ffmpeg\Unit;

use Drupal\Core\Cache\MemoryBackendFactory;
use Drupal\php_ffmpeg\PHPFFMpegCache;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for PHPFFMpegCache.
 *
 * @group php_ffmp
 */
class PHPFFMpegCacheTest extends UnitTestCase {

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $backend;

  /**
   * Random prefix.
   *
   * @var string
   */
  protected $prefix;

  /**
   * Cache.
   *
   * @var \Drupal\php_ffmpeg\PHPFFMpegCache
   */
  protected $cache;


  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['php_ffmpeg'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->backend = (new MemoryBackendFactory())->get('php_ffmpeg');
    $this->prefix = $this->randomMachineName();
    $this->cache = new PHPFFMpegCache($this->backend, $this->prefix);
  }

  /**
   * Test for PHPFFMpeg::get().
   */
  public function testFetch() {
    $cid = $this->randomMachineName();
    $value = $this->randomMachineName();
    $this->backend->set("{$this->prefix}:{$cid}", $value);
    self::assertEquals($value, $this->cache->getItem($cid)->get(),'PHPFFMpeg::get() should return the value stored in the backend when it exists.');
    $this->assertFalse($this->cache->getItem($this->randomMachineName())->isHit(), 'PHPFFMpeg::get() should return FALSE when no value exist in the backend.');
  }

  /**
   * Test for PHPFFMpeg::contains().
   */
  public function testContains() {
    $cid = $this->randomMachineName();
    $value = $this->randomMachineName();
    $this->backend->set("{$this->prefix}:{$cid}", $value);
    self::assertTrue($this->cache->hasItem($cid), 'PHPFFMpeg::contains() should return TRUE when a value exists in the backend.');
    self::assertFalse($this->cache->hasItem($this->randomMachineName()), 'PHPFFMpeg::contains() should return FALSE when no value exist in the backend.');
  }

  /**
   * Test for PHPFFMpeg::save().
   */
  public function testSave() {
    $cid = $this->randomMachineName();
    $value = $this->randomMachineName();
    $item = $this->cache->getItem($cid);
    $item->set($value);
    $this->cache->save($item);
    self::assertEquals($value, $this->backend->get("{$this->prefix}:{$cid}")->data, 'PHPFFMpeg::save() should set the value in the backend.');
  }

  /**
   * Test for PHPFFMpeg::delete().
   */
  public function testDelete() {
    $cid = $this->randomMachineName();
    $value = $this->randomMachineName();
    $this->backend->set("{$this->prefix}:{$cid}", $value);
    $this->cache->deleteItem($cid);
    self::assertFalse($this->backend->get("{$this->prefix}:{$cid}"), 'PHPFFMpeg::delete() should clear the value in the backend.');
  }

}
